/*global $, document */
$(document).ready(function () {
    'use strict';
    //
    $('.footer button').click(function () {
       $('.footer .view').fadeIn();
    });
    //
    $('.imageGallery1 a').simpleLightbox();
});